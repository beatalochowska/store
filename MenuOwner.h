#ifndef STORE_MENUOWNER_H
#define STORE_MENUOWNER_H

#include <iostream>
#include <fstream>
#include <vector>
#include "BoardGame.h"

class MenuOwner {
    std::string gameTitle;
    std::vector<BoardGame> boardGames;
    char choice;
public:
    MenuOwner();
    ~MenuOwner();
    void addGame(std::string);
    void eraseGame(std::string);
    void checkStatus();




};


#endif //STORE_MENUOWNER_H
