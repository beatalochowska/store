#include <iostream>
#include <vector>
#include "login.h"
#include "user.h"
#include <cstdlib>
#include "conio.h"
#include "helper.h"
#include "MenuUser.h"
#include "MenuOwner.h"
#include "BoardGame.h"


MenuOwner::MenuOwner(){
    char choice;

    std::cout << "Co chcialbys teraz zrobic?" << std::endl;
    std::cout << "1. Sprawdz obecny stan Twojego konta" << std::endl;
    std::cout << "2. Dodaj planszowke" << std::endl;
    std::cout << "3. Usun planszowke" << std::endl;
    std::cout << "4. Wyloguj sie" << std::endl;
    std::cout << "5. Wyjdz z programu" << std::endl;

    while(true) {


        std::cout << std::endl;
        std::cin >> choice;

        switch (choice) {
            case '1':
                std::cout << "Sprawdzam status";
                break;
            case '2':
                MenuOwner::addGame(Helper::askQuestion("Podaj nazwe planszowki do dodania: "));
                break;
            case '3':

                MenuOwner::eraseGame(Helper::askQuestion("Podaj nazwe planszowki do usuniecia: "));
                break;
            case '4':
                std::cout << "Wylogowany\n";
                Helper::sessionLogin("Witaj na stronie logowania");
                break;
            case '5':
                exit(0);
            default:
                std::cout << "nie ma takiej opcji";
                break;
        }

    }
};
void MenuOwner::addGame(std::string gameTitle){
    BoardGame game(gameTitle);
    this->boardGames.push_back(game); // po jaka cholere to this?
    std::cout << "Gra " << gameTitle << " zostala pomyslnie dodana" << std::endl;
    std::cout << "Ilosc elementow w vektorze: " << boardGames.size();
};

void MenuOwner::eraseGame(std::string gameTitle){

    for(int i = 0; i < boardGames.size(); i++) {
        if (boardGames[i].getTitle() == gameTitle) {
            this->boardGames.erase(boardGames.begin()+i);
            std::cout << "Gra " << gameTitle << " zostala pomyslnie usunieta" << std::endl;
            std::cout << "Ilosc elementow w vektorze: " << boardGames.size();
        }
        else{
            int absent = 0;
            absent++;
            if (absent == boardGames.size()) {
                printf("Taka gra nie istnieje");
            }
        }
    }
};

MenuOwner::~MenuOwner(){};



