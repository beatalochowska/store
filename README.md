# Boardgames rental

## Program created using C++

User can sign up and sign in as a customer or as a rental company.
As a customer:

- search for boardgames
- rent boardgames
- return boardgames
- search for rental companies

As a rental company:

- add new boardgames
- erease boardgames
- search for boardgame
- check a status of any boardgame

