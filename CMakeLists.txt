cmake_minimum_required(VERSION 3.12)
project(Store)

set(CMAKE_CXX_STANDARD 14)

add_executable(Store main.cpp user.cpp user.h helper.cpp helper.h login.cpp login.h MenuUser.cpp MenuUser.h MenuOwner.cpp MenuOwner.h BoardGame.cpp BoardGame.h)