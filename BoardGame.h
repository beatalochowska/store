#ifndef STORE_BOARDGAME_H
#define STORE_BOARDGAME_H

#include <iostream>
#include <fstream>

class BoardGame {

    std::string gameTitle;
    int id;
public:
    BoardGame(std::string);
    ~BoardGame();
    std::string getTitle();
};


#endif //STORE_BOARDGAME_H
