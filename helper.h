#ifndef STORE_HELPER_H
#define STORE_HELPER_H
#include <iostream>
#include <fstream>

class Helper {
    std::string dataTemp;
    std::string welcome;

public:


    Helper();
    ~Helper();

    static int checkLineNr(std::string fileName, std::string data_valid);
    static std::string askQuestion(std::string);
    static void saveData(std::string file, std::string data1);
    static void sessionLogin(std::string welcome);

};


#endif //STORE_HELPER_H
