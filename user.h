
#ifndef STORE_USER_H
#define STORE_USER_H

#include <iostream>
#include <fstream>


class User
{
    static int id;
    std::string login;
    std::string password;
    std::string fileLogin;
    std::string filePassword;

public:

    User();
    ~User();
    static void signIn(std::string fileLogin1, std::string fileLogin2, std::string filePassword);


};


#endif //STORE_USER_H
