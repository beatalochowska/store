#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include "login.h"
#include "user.h"
#include "helper.h"
#include "MenuUser.h"
#include "MenuOwner.h"
#include "BoardGame.h"

User::User() {
    char choiceUser;
    std::cout << "Chcesz grac, czy zaoferowac gry? (g/z) " << std::endl;
    std::cin >> choiceUser;

    if (choiceUser == 'g') {
        User::signIn("customerLogin.txt","pubLogin.txt", "customerPassword.txt");
        Login customerLog;
    }
    else if (choiceUser == 'z'){
        User::signIn("pubLogin.txt", "customerLogin.txt", "pubPassword.txt");
        Login pubLog;
    }
};

void User::signIn(std::string fileLogin1, std::string fileLogin2, std::string filePassword){

    std::string loginTemp = Helper::askQuestion("Podaj login: ");

    std::fstream file1;
    std::fstream file2;
    file1.open(fileLogin1, std::ios::in);
    file2.open(fileLogin2, std::ios::in);

    if (file1.good() && file2.good()) {
        while ((Helper::checkLineNr(fileLogin1, loginTemp) > 0) || (Helper::checkLineNr(fileLogin2, loginTemp) > 0)){
            std::cout << "Podany login juz istnieje! Rusz glowa, wymysl cos innego! " << std::endl;
            loginTemp = Helper::askQuestion("Podaj login: ");
        }
    }

    Helper::saveData(fileLogin1, loginTemp);

    std::string password = Helper::askQuestion("Podaj haslo: ");
    Helper::saveData(filePassword, password);


    std::cout << "Gratulacje " << loginTemp << "! Udalo Ci sie zarejestrowac!" << std::endl;
    std::cout << "Teraz zaloguj sie: " << std::endl;
};

User::~User() {};





